namespace GraphQLAPI.GraphQL;

public class ReservationInput
{
    public string EmailAddress { get; set; }

    public int FoodPacketId { get; set; }
}
