using Core.Domain;
using Core.DomainServices;

namespace GraphQLAPI.GraphQL;

public class FoodPacketQuery
{
    [UseFiltering]
    [UseSorting]
    public IEnumerable<FoodPacket> GetAllFoodPackets([Service] IFoodPacketRepository foodPacketRepository)
    {
        return foodPacketRepository.GetAllFoodPackets();
    }

    public FoodPacket GetFoodPacketById([Service] IFoodPacketRepository foodPacketRepository, int id)
    {
        return foodPacketRepository.GetFoodPacketById(id);
    }
}
