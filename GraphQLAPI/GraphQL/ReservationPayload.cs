using Core.Domain;

namespace GraphQLAPI.GraphQL;

public class ReservationPayload
{
    public FoodPacket FoodPacket { get; set; }

    public string? Error { get; set; }
}
