using Core.DomainServices;

namespace GraphQLAPI.GraphQL;

public class FoodPacketMutation
{
    public ReservationPayload AddReservation([Service] IStudentRepository studentRepository,
        [Service] IFoodPacketRepository foodPacketRepository, ReservationInput reservationInput)
    {
        var student = studentRepository.GetStudentByEmail(reservationInput.EmailAddress);
        var foodPacket = foodPacketRepository.GetFoodPacketById(reservationInput.FoodPacketId);

        // check if packet hasnt been reserved yet
        if (foodPacket.ReservedBy != null)
        {
            return new ReservationPayload {Error = "This package has already been reserved", FoodPacket = foodPacket};
        }

        // check if user is 18 if packet contains alcohol
        var userAgePlus18 = student.Birthday.AddYears(18);
        if ((bool) foodPacket.Is18Plus && userAgePlus18.Date <= foodPacket.PickupTime.Value.Date)
            return new ReservationPayload {Error = "This packet can only be reserved by adults"};

        // check if user doesnt have reservation on pickuptime
        var foodPackets = foodPacketRepository.GetAllFoodPackets();
        var studentReservations = foodPackets.Where(p => p.ReservedBy == student).ToList();
        if (studentReservations.Exists(m => m.PickupTime.Value.Date == foodPacket.PickupTime.Value.Date))
            return new ReservationPayload {Error = "You can only have 1 reservation per day"};

        foodPacketRepository.SetReservation(foodPacket, student);

        var payload = new ReservationPayload {Error = "", FoodPacket = foodPacket};

        return payload;
    }
}
