using Core.Domain;
using Core.DomainServices;

namespace Infrastructure;

public class StudentRepository : IStudentRepository
{
    private readonly FoodWasteDbContext _dbContext;

    public StudentRepository(FoodWasteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void AddStudent(Student student)
    {
        _dbContext.Students.Add(student);
        _dbContext.SaveChanges();
    }

    public void UpdateStudent(Student student)
    {
        _dbContext.Students.Update(student);
        _dbContext.SaveChanges();
    }

    public Student GetStudentById(int id)
    {
        return _dbContext.Students.SingleOrDefault(s => s.Id == id) ?? throw new InvalidOperationException();
    }

    public IEnumerable<Student> GetAllStudents()
    {
        return _dbContext.Students.ToList();
    }

    public Student GetStudentByEmail(string email)
    {
        return _dbContext.Students.SingleOrDefault(s => s.EmailAddress == email) ??
               throw new InvalidOperationException();
    }
}