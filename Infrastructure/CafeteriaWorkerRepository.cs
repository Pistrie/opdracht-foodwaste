using Core.Domain;
using Core.DomainServices;

namespace Infrastructure;

public class CafeteriaWorkerRepository : ICafeteriaWorkerRepository
{
    private readonly FoodWasteDbContext _dbContext;

    public CafeteriaWorkerRepository(FoodWasteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void AddCafeteriaWorker(CafeteriaWorker cafeteriaWorker)
    {
        _dbContext.CafeteriaWorkers.Add(cafeteriaWorker);
        _dbContext.SaveChanges();
    }

    public void UpdateCafeteriaWorker(CafeteriaWorker cafeteriaWorker)
    {
        _dbContext.CafeteriaWorkers.Update(cafeteriaWorker);
        _dbContext.SaveChanges();
    }

    public CafeteriaWorker GetCafeteriaWorkerById(int id)
    {
        return _dbContext.CafeteriaWorkers.SingleOrDefault(cw => cw.Id == id);
    }

    public CafeteriaWorker GetCafeteriaWorkerByEmail(string email)
    {
        return _dbContext.CafeteriaWorkers.SingleOrDefault(cw => cw.EmailAddress == email);
    }

    public IEnumerable<CafeteriaWorker> GetAllCafeteriaWorkers()
    {
        return _dbContext.CafeteriaWorkers.ToList();
    }
}