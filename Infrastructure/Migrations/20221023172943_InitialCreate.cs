﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cafeterias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    City = table.Column<int>(type: "int", nullable: false),
                    CafeteriaLocation = table.Column<int>(type: "int", nullable: false),
                    IsHotMealServed = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cafeterias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CafeteriaWorkers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeNumber = table.Column<int>(type: "int", nullable: false),
                    EmailAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CafeteriaLocation = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CafeteriaWorkers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Alcoholic = table.Column<bool>(type: "bit", nullable: false),
                    ProfilePicture = table.Column<byte[]>(type: "varbinary(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Birthday = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StudentNumber = table.Column<int>(type: "int", nullable: false),
                    EmailAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SchoolCity = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FoodPackets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    City = table.Column<int>(type: "int", nullable: true),
                    Location = table.Column<int>(type: "int", nullable: true),
                    PickupTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PickupExpirationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is18Plus = table.Column<bool>(type: "bit", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: true),
                    MealType = table.Column<int>(type: "int", nullable: true),
                    ReservedById = table.Column<int>(type: "int", nullable: true),
                    IsCollected = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodPackets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodPackets_Students_ReservedById",
                        column: x => x.ReservedById,
                        principalTable: "Students",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FoodPacketsProducts",
                columns: table => new
                {
                    ProductsId = table.Column<int>(type: "int", nullable: false),
                    FoodPacketsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodPacketsProducts", x => new { x.ProductsId, x.FoodPacketsId });
                    table.ForeignKey(
                        name: "FK_FoodPacketsProducts_FoodPackets_FoodPacketsId",
                        column: x => x.FoodPacketsId,
                        principalTable: "FoodPackets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FoodPacketsProducts_Products_ProductsId",
                        column: x => x.ProductsId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CafeteriaWorkers",
                columns: new[] { "Id", "CafeteriaLocation", "EmailAddress", "EmployeeNumber" },
                values: new object[,]
                {
                    { 1, 0, "p.paaltjens@avans.nl", 1122 },
                    { 2, 1, "e.terpstra@avans.nl", 2233 }
                });

            migrationBuilder.InsertData(
                table: "Cafeterias",
                columns: new[] { "Id", "CafeteriaLocation", "City", "IsHotMealServed" },
                values: new object[,]
                {
                    { 1, 0, 0, true },
                    { 2, 1, 0, false }
                });

            migrationBuilder.InsertData(
                table: "FoodPackets",
                columns: new[] { "Id", "City", "Is18Plus", "IsCollected", "Location", "MealType", "Name", "PickupExpirationTime", "PickupTime", "Price", "ReservedById" },
                values: new object[,]
                {
                    { 1, 0, false, false, 0, 0, "Remaining paninis", new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified), 5, null },
                    { 2, 0, false, false, 1, 1, "Hot dinner", new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified), 8, null }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Alcoholic", "Name", "ProfilePicture" },
                values: new object[,]
                {
                    { 1, false, "Panini spicy chicken", null },
                    { 2, false, "Big Bowl of tomato soup", null },
                    { 3, true, "Beer", null },
                    { 4, true, "Wine", null }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "Birthday", "EmailAddress", "Name", "PhoneNumber", "SchoolCity", "StudentNumber" },
                values: new object[,]
                {
                    { 1, new DateTime(2000, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "s.roos2@student.avans.nl", "Sylvester Roos", "06 12345678", 0, 1234567 },
                    { 2, new DateTime(2006, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "j.janssen@student.avans.nl", "Jan Janssen", "06 22225678", 0, 1122345 }
                });

            migrationBuilder.InsertData(
                table: "FoodPacketsProducts",
                columns: new[] { "FoodPacketsId", "ProductsId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "FoodPacketsProducts",
                columns: new[] { "FoodPacketsId", "ProductsId" },
                values: new object[] { 2, 2 });

            migrationBuilder.CreateIndex(
                name: "IX_FoodPackets_ReservedById",
                table: "FoodPackets",
                column: "ReservedById");

            migrationBuilder.CreateIndex(
                name: "IX_FoodPacketsProducts_FoodPacketsId",
                table: "FoodPacketsProducts",
                column: "FoodPacketsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cafeterias");

            migrationBuilder.DropTable(
                name: "CafeteriaWorkers");

            migrationBuilder.DropTable(
                name: "FoodPacketsProducts");

            migrationBuilder.DropTable(
                name: "FoodPackets");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
