﻿// <auto-generated />
using System;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Infrastructure.Migrations
{
    [DbContext(typeof(FoodWasteDbContext))]
    [Migration("20221023172943_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("Core.Domain.Cafeteria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("CafeteriaLocation")
                        .HasColumnType("int");

                    b.Property<int>("City")
                        .HasColumnType("int");

                    b.Property<bool>("IsHotMealServed")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.ToTable("Cafeterias");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CafeteriaLocation = 0,
                            City = 0,
                            IsHotMealServed = true
                        },
                        new
                        {
                            Id = 2,
                            CafeteriaLocation = 1,
                            City = 0,
                            IsHotMealServed = false
                        });
                });

            modelBuilder.Entity("Core.Domain.CafeteriaWorker", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("CafeteriaLocation")
                        .HasColumnType("int");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("EmployeeNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("CafeteriaWorkers");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CafeteriaLocation = 0,
                            EmailAddress = "p.paaltjens@avans.nl",
                            EmployeeNumber = 1122
                        },
                        new
                        {
                            Id = 2,
                            CafeteriaLocation = 1,
                            EmailAddress = "e.terpstra@avans.nl",
                            EmployeeNumber = 2233
                        });
                });

            modelBuilder.Entity("Core.Domain.FoodPacket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int?>("City")
                        .HasColumnType("int");

                    b.Property<bool?>("Is18Plus")
                        .HasColumnType("bit");

                    b.Property<bool>("IsCollected")
                        .HasColumnType("bit");

                    b.Property<int?>("Location")
                        .HasColumnType("int");

                    b.Property<int?>("MealType")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("PickupExpirationTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("PickupTime")
                        .HasColumnType("datetime2");

                    b.Property<int?>("Price")
                        .HasColumnType("int");

                    b.Property<int?>("ReservedById")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ReservedById");

                    b.ToTable("FoodPackets");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            City = 0,
                            Is18Plus = false,
                            IsCollected = false,
                            Location = 0,
                            MealType = 0,
                            Name = "Remaining paninis",
                            PickupExpirationTime = new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified),
                            PickupTime = new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified),
                            Price = 5
                        },
                        new
                        {
                            Id = 2,
                            City = 0,
                            Is18Plus = false,
                            IsCollected = false,
                            Location = 1,
                            MealType = 1,
                            Name = "Hot dinner",
                            PickupExpirationTime = new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified),
                            PickupTime = new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified),
                            Price = 8
                        });
                });

            modelBuilder.Entity("Core.Domain.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<bool>("Alcoholic")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<byte[]>("ProfilePicture")
                        .HasColumnType("varbinary(max)");

                    b.HasKey("Id");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alcoholic = false,
                            Name = "Panini spicy chicken"
                        },
                        new
                        {
                            Id = 2,
                            Alcoholic = false,
                            Name = "Big Bowl of tomato soup"
                        },
                        new
                        {
                            Id = 3,
                            Alcoholic = true,
                            Name = "Beer"
                        },
                        new
                        {
                            Id = 4,
                            Alcoholic = true,
                            Name = "Wine"
                        });
                });

            modelBuilder.Entity("Core.Domain.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<DateTime>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SchoolCity")
                        .HasColumnType("int");

                    b.Property<int>("StudentNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Students");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Birthday = new DateTime(2000, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            EmailAddress = "s.roos2@student.avans.nl",
                            Name = "Sylvester Roos",
                            PhoneNumber = "06 12345678",
                            SchoolCity = 0,
                            StudentNumber = 1234567
                        },
                        new
                        {
                            Id = 2,
                            Birthday = new DateTime(2006, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            EmailAddress = "j.janssen@student.avans.nl",
                            Name = "Jan Janssen",
                            PhoneNumber = "06 22225678",
                            SchoolCity = 0,
                            StudentNumber = 1122345
                        });
                });

            modelBuilder.Entity("FoodPacketsProducts", b =>
                {
                    b.Property<int>("ProductsId")
                        .HasColumnType("int");

                    b.Property<int>("FoodPacketsId")
                        .HasColumnType("int");

                    b.HasKey("ProductsId", "FoodPacketsId");

                    b.HasIndex("FoodPacketsId");

                    b.ToTable("FoodPacketsProducts");

                    b.HasData(
                        new
                        {
                            ProductsId = 1,
                            FoodPacketsId = 1
                        },
                        new
                        {
                            ProductsId = 2,
                            FoodPacketsId = 2
                        });
                });

            modelBuilder.Entity("Core.Domain.FoodPacket", b =>
                {
                    b.HasOne("Core.Domain.Student", "ReservedBy")
                        .WithMany()
                        .HasForeignKey("ReservedById");

                    b.Navigation("ReservedBy");
                });

            modelBuilder.Entity("FoodPacketsProducts", b =>
                {
                    b.HasOne("Core.Domain.FoodPacket", null)
                        .WithMany()
                        .HasForeignKey("FoodPacketsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Domain.Product", null)
                        .WithMany()
                        .HasForeignKey("ProductsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
