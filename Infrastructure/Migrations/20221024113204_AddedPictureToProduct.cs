﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    public partial class AddedPictureToProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PictureFormat",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "FoodPackets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PickupExpirationTime", "PickupTime" },
                values: new object[] { new DateTime(2022, 10, 25, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 25, 17, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "FoodPackets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PickupExpirationTime", "PickupTime" },
                values: new object[] { new DateTime(2022, 10, 25, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 25, 17, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PictureFormat",
                table: "Products");

            migrationBuilder.UpdateData(
                table: "FoodPackets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PickupExpirationTime", "PickupTime" },
                values: new object[] { new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "FoodPackets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PickupExpirationTime", "PickupTime" },
                values: new object[] { new DateTime(2022, 10, 24, 19, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 10, 24, 17, 0, 0, 0, DateTimeKind.Unspecified) });
        }
    }
}
