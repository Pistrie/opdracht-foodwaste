using Core.Domain;
using Core.DomainServices;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class FoodPacketRepository : IFoodPacketRepository
{
    private readonly FoodWasteDbContext _dbContext;

    public FoodPacketRepository(FoodWasteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void AddFoodPacket(FoodPacket foodPacket)
    {
        _dbContext.FoodPackets.Add(foodPacket);
        _dbContext.SaveChanges();
    }

    public void UpdateFoodPacket(FoodPacket updatedFoodPacket)
    {
        var originalFoodPacket = _dbContext.FoodPackets.Include(fp => fp.Products)
            .FirstOrDefault(fp => fp.Id == updatedFoodPacket.Id);
        if (originalFoodPacket != null)
        {
            originalFoodPacket.Products.Any(p => originalFoodPacket.Products.Remove(p));

            originalFoodPacket.Products = updatedFoodPacket.Products;
            originalFoodPacket.City = updatedFoodPacket.City;
            originalFoodPacket.Location = updatedFoodPacket.Location;
            originalFoodPacket.Name = updatedFoodPacket.Name;
            originalFoodPacket.Price = updatedFoodPacket.Price;
            originalFoodPacket.Is18Plus = updatedFoodPacket.Is18Plus;
            originalFoodPacket.IsCollected = updatedFoodPacket.IsCollected;
            originalFoodPacket.MealType = updatedFoodPacket.MealType;
            originalFoodPacket.PickupTime = updatedFoodPacket.PickupTime;
            originalFoodPacket.ReservedBy = updatedFoodPacket.ReservedBy;
            originalFoodPacket.PickupExpirationTime = updatedFoodPacket.PickupExpirationTime;

            _dbContext.FoodPackets.Update(originalFoodPacket);
        }

        _dbContext.SaveChanges();
    }

    public void SetReservation(FoodPacket foodPacket, Student reservedBy)
    {
        foodPacket.ReservedBy = reservedBy;

        _dbContext.FoodPackets.Update(foodPacket);
        _dbContext.SaveChanges();
    }

    public void ClearReservation(FoodPacket foodPacket)
    {
        foodPacket.ReservedBy = null;

        _dbContext.FoodPackets.Update(foodPacket);
        _dbContext.SaveChanges();
    }

    public FoodPacket GetFoodPacketById(int id)
    {
        return _dbContext.FoodPackets.Include(fp => fp.Products).Include(fp => fp.ReservedBy).SingleOrDefault(fp => fp.Id == id) ??
               throw new InvalidOperationException();
    }

    public IEnumerable<FoodPacket> GetAllFoodPackets()
    {
        return _dbContext.FoodPackets.Include(fp => fp.Products).Include(fp => fp.ReservedBy).ToList();
    }

    public void DeleteFoodPacket(FoodPacket foodPacket)
    {
        _dbContext.FoodPackets.Remove(foodPacket);
        _dbContext.SaveChanges();
    }
}
