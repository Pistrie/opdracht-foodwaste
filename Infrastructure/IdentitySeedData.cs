using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure;

public class IdentitySeedData
{
    private const string Worker1Email = "p.paaltjens@avans.nl";
    private const string Worker1Password = "Secret123$";

    private const string Worker2Email = "e.terpstra@avans.nl";
    private const string Worker2Password = "Secret123$";

    private const string Student1Email = "s.roos2@student.avans.nl";
    private const string Student1Password = "Secret123$";

    private const string Student2Email = "j.janssen@student.avans.nl";
    private const string Student2Password = "Secret123$";

    public static async Task EnsurePopulated(UserManager<IdentityUser> userManager)
    {
        var worker1 = await userManager.FindByEmailAsync(Worker1Email);
        if (worker1 == null)
        {
            worker1 = new IdentityUser(Worker1Email);
            await userManager.CreateAsync(worker1, Worker1Password);
            await userManager.SetEmailAsync(worker1, Worker1Email);
            await userManager.AddClaimAsync(worker1, new Claim("CafeteriaWorker", "true"));
        }

        var worker2 = await userManager.FindByIdAsync(Worker2Email);
        if (worker2 == null)
        {
            worker2 = new IdentityUser("e.terpstra@avans.nl");
            await userManager.CreateAsync(worker2, Worker2Password);
            await userManager.SetEmailAsync(worker2, Worker2Email);
            await userManager.AddClaimAsync(worker2, new Claim("CafeteriaWorker", "true"));
        }

        var student1 = await userManager.FindByIdAsync(Student1Email);
        if (student1 == null)
        {
            student1 = new IdentityUser("s.roos2@student.avans.nl");
            await userManager.CreateAsync(student1, Student1Password);
            await userManager.SetEmailAsync(student1, Student1Email);
            await userManager.AddClaimAsync(student1, new Claim("Student", "true"));
        }

        var student2 = await userManager.FindByIdAsync(Student2Email);
        if (student2 == null)
        {
            student2 = new IdentityUser("j.janssen@student.avans.nl");
            await userManager.CreateAsync(student2, Student2Password);
            await userManager.SetEmailAsync(student2, Student2Email);
            await userManager.AddClaimAsync(student2, new Claim("Student", "true"));
        }
    }
}
