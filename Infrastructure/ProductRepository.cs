using Core.Domain;
using Core.DomainServices;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class ProductRepository : IProductRepository
{
    private readonly FoodWasteDbContext _dbContext;

    public ProductRepository(FoodWasteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void AddProduct(Product product)
    {
        _dbContext.Products.Add(product);
        _dbContext.SaveChanges();
    }

    public void UpdateProduct(Product updatedProduct)
    {
        var originalProduct = _dbContext.Products.FirstOrDefault(p => p.Id == updatedProduct.Id);
        if (originalProduct != null)
        {
            originalProduct.Id = updatedProduct.Id;
            originalProduct.Name = updatedProduct.Name;
            originalProduct.ProfilePicture = updatedProduct.ProfilePicture;
            originalProduct.Alcoholic = updatedProduct.Alcoholic;

            _dbContext.Products.Update(originalProduct);
        }

        _dbContext.SaveChanges();
    }

    public Product GetProductById(int id)
    {
        return _dbContext.Products.Include(p => p.FoodPackets).ThenInclude(fp => fp.ReservedBy)
            .SingleOrDefault(p => p.Id == id);
    }

    public IEnumerable<Product> GetAllProducts()
    {
        return _dbContext.Products.Include(p => p.FoodPackets).ThenInclude(fp => fp.ReservedBy).ToList();
    }

    public void DeleteProduct(Product product)
    {
        _dbContext.Products.Remove(product);
        _dbContext.SaveChanges();
    }
}
