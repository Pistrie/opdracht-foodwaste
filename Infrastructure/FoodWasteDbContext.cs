using Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class FoodWasteDbContext : DbContext
{
    public FoodWasteDbContext(DbContextOptions<FoodWasteDbContext> contextOptions) : base(contextOptions)
    {
    }

    public DbSet<Cafeteria> Cafeterias { get; set; }
    public DbSet<CafeteriaWorker> CafeteriaWorkers { get; set; }
    public DbSet<FoodPacket> FoodPackets { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Student> Students { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var cafeterias = new List<Cafeteria>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LA,
                IsHotMealServed = true
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LD,
                IsHotMealServed = false
            }
        };

        var cafeteriaWorkers = new List<CafeteriaWorker>
        {
            new()
            {
                Id = 1,
                CafeteriaLocation = CafeteriaLocationEnum.LA,
                EmployeeNumber = 1122,
                EmailAddress = "p.paaltjens@avans.nl"
            },
            new()
            {
                Id = 2,
                CafeteriaLocation = CafeteriaLocationEnum.LD,
                EmployeeNumber = 2233,
                EmailAddress = "e.terpstra@avans.nl"
            }
        };

        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPackets = new List<FoodPacket>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LA,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.Bread,
                Name = "Remaining paninis",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 5
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LD,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.HotDinner,
                Name = "Hot dinner",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 8
            }
        };

        var products = new List<Product>
        {
            new()
            {
                Id = 1,
                Alcoholic = false,
                Name = "Panini spicy chicken"
            },
            new()
            {
                Id = 2,
                Alcoholic = false,
                Name = "Big Bowl of tomato soup"
            },
            new()
            {
                Id = 3,
                Alcoholic = true,
                Name = "Beer"
            },
            new()
            {
                Id = 4,
                Alcoholic = true,
                Name = "Wine"
            }
        };

        var students = new List<Student>
        {
            new()
            {
                Id = 1,
                Birthday = new DateTime(2000, 11, 23),
                EmailAddress = "s.roos2@student.avans.nl",
                Name = "Sylvester Roos",
                PhoneNumber = "06 12345678",
                SchoolCity = CafeteriaCityEnum.Breda,
                StudentNumber = 1234567
            },
            new()
            {
                Id = 2,
                Birthday = new DateTime(2006, 10, 17),
                EmailAddress = "j.janssen@student.avans.nl",
                Name = "Jan Janssen",
                PhoneNumber = "06 22225678",
                SchoolCity = CafeteriaCityEnum.Breda,
                StudentNumber = 1122345
            }
        };

        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Cafeteria>().HasData(cafeterias);

        modelBuilder.Entity<CafeteriaWorker>().HasData(cafeteriaWorkers);

        modelBuilder.Entity<Product>().HasData(products);

        modelBuilder.Entity<FoodPacket>().HasData(foodPackets);
        modelBuilder.Entity<FoodPacket>()
            .HasMany(fp => fp.Products)
            .WithMany(p => p.FoodPackets)
            .UsingEntity<Dictionary<string, object>>(
                "FoodPacketsProducts",
                r => r.HasOne<Product>().WithMany().HasForeignKey("ProductsId"),
                l => l.HasOne<FoodPacket>().WithMany().HasForeignKey("FoodPacketsId"),
                je =>
                {
                    je.HasKey("ProductsId", "FoodPacketsId");
                    je.HasData(
                        new {ProductsId = 1, FoodPacketsId = 1},
                        new {ProductsId = 2, FoodPacketsId = 2});
                });

        modelBuilder.Entity<Student>().HasData(students);
    }
}
