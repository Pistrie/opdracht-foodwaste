using Core.Domain;
using Core.DomainServices;

namespace Infrastructure;

public class CafeteriaRepository : ICafeteriaRepository
{
    private readonly FoodWasteDbContext _dbContext;


    public CafeteriaRepository(FoodWasteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Cafeteria GetCafeteriaById(int id)
    {
        return _dbContext.Cafeterias.SingleOrDefault(c => c.Id == id);
    }

    public IEnumerable<Cafeteria> GetAllCafeterias()
    {
        return _dbContext.Cafeterias.ToList();
    }
}