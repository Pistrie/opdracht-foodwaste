using System.ComponentModel.DataAnnotations;

namespace Portal.Models;

public class AddNewProductViewModel
{
    public int Id { get; set; }

    public string Name { get; set; }

    public bool Alcoholic { get; set; }

    [Required(ErrorMessage = "Please upload a picture of the product")]
    public IFormFile ProfilePicture { get; set; }
}