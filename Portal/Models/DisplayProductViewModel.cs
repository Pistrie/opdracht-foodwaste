namespace Portal.Models;

public class DisplayProductViewModel
{
    public string Name { get; set; }

    public bool Alcoholic { get; set; }

    public string? ProfilePicture { get; set; }

    public string? PictureFormat { get; set; }
}
