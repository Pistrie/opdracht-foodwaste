using System.ComponentModel.DataAnnotations;
using Core.Domain;

namespace Portal.Models;

public class AddNewPacketViewModel
{
    private static readonly DateTime Today = DateTime.Now;

    public IEnumerable<CheckboxOption>? ProductsAsCheckboxOption { get; set; }

    // binding for the checkbox list
    [Required(ErrorMessage = "Please add at least one product to the food packet")]
    public IEnumerable<int> IdsOfChosenProducts { get; set; }

    public CafeteriaCityEnum City { get; set; }

    public string Name { get; set; }

    public DateTime PickupTime { get; set; } =
        new(Today.Year, Today.Month, Today.Day, Today.Hour, Today.Minute, 0);

    public DateTime PickupExpirationTime { get; set; } =
        new(Today.Year, Today.Month, Today.Day, Today.Hour, Today.Minute, 0);

    public int Price { get; set; }

    public MealType MealType { get; set; }
}
