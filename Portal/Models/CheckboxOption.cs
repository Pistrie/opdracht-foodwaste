namespace Portal.Models;

public class CheckboxOption
{
    public int Id { get; set; }

    public string Name { get; set; }

    public bool IsChecked { get; set; }
}
