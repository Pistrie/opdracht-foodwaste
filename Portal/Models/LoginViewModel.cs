using System.ComponentModel.DataAnnotations;

namespace Portal.Models;

public class LoginViewModel
{
    [Required]
    [EmailAddress]
    [Display(Name = "Email address")]
    public string EmailAddress { get; set; }

    [Required] public string Password { get; set; }
}
