using Core.Domain;

namespace Portal.Models;

public class StudentIndexViewModel
{
    public IEnumerable<FoodPacket> FoodPackets { get; set; }

    public Student User { get; set; }

    public DateTime UserAgePlus18 { get; set; }

    public List<FoodPacket> UserReservations { get; set; }

    public bool IsReservationThatDay(FoodPacket foodPacketToReserve)
    {
        return UserReservations.Exists(m => m.PickupTime.Value.Date == foodPacketToReserve.PickupTime.Value.Date);
    }
}
