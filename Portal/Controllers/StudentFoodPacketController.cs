using Core.DomainServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Portal.Models;

namespace Portal.Controllers;

[Authorize(Policy = "Student")]
public class StudentFoodPacketController : Controller
{
    private readonly IFoodPacketRepository _foodPacketRepository;
    private readonly ILogger<StudentFoodPacketController> _logger;
    private readonly IStudentRepository _studentRepository;
    private readonly UserManager<IdentityUser> _userManager;

    public StudentFoodPacketController(ILogger<StudentFoodPacketController> logger,
        IFoodPacketRepository foodPacketRepository,
        UserManager<IdentityUser> userManager, IStudentRepository studentRepository)
    {
        _logger = logger;
        _foodPacketRepository = foodPacketRepository;
        _userManager = userManager;
        _studentRepository = studentRepository;
    }

    public async Task<ViewResult> Index()
    {
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var student = _studentRepository.GetStudentByEmail(user.Email);
        var foodPackets = _foodPacketRepository.GetAllFoodPackets().ToList();
        var studentReservations = foodPackets.Where(p => p.ReservedBy == student).ToList();

        if (TempData["PacketIsReserved"] != null)
        {
            ViewBag.PacketIsReserved = TempData["PacketIsReserved"];
            TempData.Clear();
        }

        return View(new StudentIndexViewModel
        {
            FoodPackets = _foodPacketRepository.GetAllFoodPackets()
                .Where(fp => fp.ReservedBy == null),
            User = student,
            UserAgePlus18 = student.Birthday.AddYears(18),
            UserReservations = studentReservations
        });
    }

    public async Task<ViewResult> ReservedPackets()
    {
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var reservedPackets = _foodPacketRepository.GetAllFoodPackets()
            .Where(fp => fp.ReservedBy?.EmailAddress == user.Email);
        return View(reservedPackets);
    }

    public async Task<RedirectToActionResult> ReserveFoodPacket(int id)
    {
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var student = _studentRepository.GetStudentByEmail(user.Email);
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        if (foodPacket.ReservedBy != null)
        {
            TempData["PacketIsReserved"] = "Apologies, but this packet has since been reserved by another student";
            return RedirectToAction("Index");
        }

        _foodPacketRepository.SetReservation(foodPacket, student);
        return RedirectToAction("Index");
    }

    public RedirectToActionResult CancelFoodPacketReservation(int id)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        _foodPacketRepository.ClearReservation(foodPacket);
        return RedirectToAction("ReservedPackets");
    }

    public ViewResult ViewFoodPacketDetails(int id)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        return View(foodPacket);
    }
}
