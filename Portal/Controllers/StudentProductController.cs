using Core.DomainServices;
using Microsoft.AspNetCore.Mvc;
using Portal.Models;

namespace Portal.Controllers;

public class StudentProductController : Controller
{
    private readonly IProductRepository _productRepository;

    public StudentProductController(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public ViewResult ViewProductDetails(int id)
    {
        var product = _productRepository.GetProductById(id);
        var displayProductViewModel = new DisplayProductViewModel
        {
            Name = product.Name,
            Alcoholic = product.Alcoholic
        };
        if (product.ProfilePicture != null)
            displayProductViewModel.ProfilePicture = Convert.ToBase64String(product.ProfilePicture);
        else product.ProfilePicture = null;
        displayProductViewModel.PictureFormat = product.PictureFormat;
        return View(displayProductViewModel);
    }
}
