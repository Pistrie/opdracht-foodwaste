﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Portal.Models;

namespace Portal.Controllers;

[Authorize]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public RedirectToActionResult Index()
    {
        var claims = User.Claims;
        foreach (var claim in claims)
            switch (claim.Type)
            {
                case "Student" when claim.Value == "true":
                    return RedirectToAction("index", "StudentFoodPacket");
                case "CafeteriaWorker" when claim.Value == "true":
                    return RedirectToAction("index", "WorkerFoodPacket");
            }

        return RedirectToAction("login", "Account");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}
