using Core.Domain;
using Core.DomainServices;
using Microsoft.AspNetCore.Mvc;
using Portal.Models;

namespace Portal.Controllers;

public class WorkerProductController : Controller
{
    private readonly IProductRepository _productRepository;

    public WorkerProductController(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public ViewResult ViewAllProducts()
    {
        var products = _productRepository.GetAllProducts();
        return View(products);
    }

    [HttpGet]
    public ViewResult AddNewProduct()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult AddNewProduct(AddNewProductViewModel addNewProductViewModel)
    {
        if (!ModelState.IsValid) return View();

        var product = new Product
        {
            Name = addNewProductViewModel.Name,
            Alcoholic = addNewProductViewModel.Alcoholic,
            PictureFormat = addNewProductViewModel.ProfilePicture.ContentType
        };

        var memoryStream = new MemoryStream();
        addNewProductViewModel.ProfilePicture.CopyTo(memoryStream);
        product.ProfilePicture = memoryStream.ToArray();

        _productRepository.AddProduct(product);
        return RedirectToAction("ViewAllProducts");
    }

    public ViewResult ViewProductDetails(int id)
    {
        var product = _productRepository.GetProductById(id);
        var displayProductViewModel = new DisplayProductViewModel
        {
            Name = product.Name,
            Alcoholic = product.Alcoholic
        };
        if (product.ProfilePicture != null)
            displayProductViewModel.ProfilePicture = Convert.ToBase64String(product.ProfilePicture);
        else product.ProfilePicture = null;
        displayProductViewModel.PictureFormat = product.PictureFormat;
        return View(displayProductViewModel);
    }

    [HttpGet]
    public IActionResult EditProductById(int id)
    {
        var product = _productRepository.GetProductById(id);
        var foodPackets = product.FoodPackets.ToList();
        if (foodPackets.Exists(fp => fp.ReservedBy != null))
        {
            TempData["PacketIsReserved"] =
                "Apologies, but this product belongs to a packet that has since been reserved by a student";
            return RedirectToAction("ViewAllProducts");
        }

        return View(new AddNewProductViewModel
        {
            Id = product.Id,
            Name = product.Name,
            Alcoholic = product.Alcoholic
        });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult EditProductById(AddNewProductViewModel addNewProductViewModel)
    {
        var productChecking = _productRepository.GetProductById(addNewProductViewModel.Id);
        var foodPackets = productChecking.FoodPackets.ToList();
        if (foodPackets.Exists(fp => fp.ReservedBy != null))
        {
            TempData["PacketIsReserved"] =
                "Apologies, but this product belongs to a packet that has since been reserved by a student";
            return RedirectToAction("ViewAllProducts");
        }

        if (!ModelState.IsValid) return View(addNewProductViewModel);

        var product = new Product
        {
            Id = addNewProductViewModel.Id,
            Name = addNewProductViewModel.Name,
            Alcoholic = addNewProductViewModel.Alcoholic,
            PictureFormat = addNewProductViewModel.ProfilePicture.ContentType
        };

        var memoryStream = new MemoryStream();
        addNewProductViewModel.ProfilePicture.CopyTo(memoryStream);
        product.ProfilePicture = memoryStream.ToArray();

        _productRepository.UpdateProduct(product);
        return RedirectToAction("ViewAllProducts");
    }

    public RedirectToActionResult DeleteProductById(int id)
    {
        var product = _productRepository.GetProductById(id);
        var foodPackets = product.FoodPackets.ToList();
        if (foodPackets.Exists(fp => fp.ReservedBy != null))
        {
            TempData["PacketIsReserved"] =
                "Apologies, but this product belongs to a packet that has since been reserved by a student";
            return RedirectToAction("ViewAllProducts");
        }

        _productRepository.DeleteProduct(product);

        return RedirectToAction("ViewAllProducts", "WorkerProduct");
    }
}
