using Core.Domain;
using Core.DomainServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Portal.Models;

namespace Portal.Controllers;

[Authorize(Policy = "CafeteriaWorker")]
public class WorkerFoodPacketController : Controller
{
    private readonly ICafeteriaRepository _cafeteriaRepository;
    private readonly ICafeteriaWorkerRepository _cafeteriaWorkerRepository;
    private readonly IFoodPacketRepository _foodPacketRepository;
    private readonly ILogger<WorkerFoodPacketController> _logger;
    private readonly IProductRepository _productRepository;
    private readonly UserManager<IdentityUser> _userManager;

    public WorkerFoodPacketController(ILogger<WorkerFoodPacketController> logger,
        IFoodPacketRepository foodPacketRepository,
        UserManager<IdentityUser> userManager, ICafeteriaWorkerRepository cafeteriaWorkerRepository,
        IProductRepository productRepository, ICafeteriaRepository cafeteriaRepository)
    {
        _logger = logger;
        _foodPacketRepository = foodPacketRepository;
        _userManager = userManager;
        _cafeteriaWorkerRepository = cafeteriaWorkerRepository;
        _productRepository = productRepository;
        _cafeteriaRepository = cafeteriaRepository;
    }

    public async Task<ViewResult> Index()
    {
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var worker = _cafeteriaWorkerRepository.GetCafeteriaWorkerByEmail(user.Email);
        var workerLocationPackets = _foodPacketRepository.GetAllFoodPackets()
            .Where(fp => fp.Location == worker.CafeteriaLocation);
        var allLocationPackets = _foodPacketRepository.GetAllFoodPackets();
        IEnumerable<IEnumerable<FoodPacket>> packetList = new List<IEnumerable<FoodPacket>>
        {
            workerLocationPackets.OrderBy(fp => fp.PickupTime),
            allLocationPackets.OrderBy(fp => fp.PickupTime)
        };

        if (TempData["PacketIsReserved"] != null)
        {
            ViewBag.PacketIsReserved = TempData["PacketIsReserved"];
            TempData.Clear();
        }

        return View(packetList);
    }

    [HttpGet]
    public ViewResult AddNewPacket()
    {
        // retrieve all products to show on the page
        var products = _productRepository.GetAllProducts();
        var checkboxOptions = products.Select(product => new CheckboxOption
            {Id = product.Id, Name = product.Name, IsChecked = false}).ToList();

        var addNewPacketViewModel = new AddNewPacketViewModel
        {
            ProductsAsCheckboxOption = checkboxOptions
        };

        return View(addNewPacketViewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> AddNewPacket(AddNewPacketViewModel addNewPacketViewModel)
    {
        // figure out which single cafeteria was chosen
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var worker = _cafeteriaWorkerRepository.GetCafeteriaWorkerByEmail(user.Email);
        var chosenCafeteria = _cafeteriaRepository.GetAllCafeterias().Where(c =>
            c.City == addNewPacketViewModel.City && c.CafeteriaLocation == worker.CafeteriaLocation);
        var enumerable = chosenCafeteria as Cafeteria[] ?? chosenCafeteria.ToArray();
        if (enumerable.Length > 1 || !enumerable.Any())
            ModelState.AddModelError(nameof(Cafeteria),
                "No cafeteria matched, or more than 1 cafeteria matched the combination of the chosen city and your location");
        if (chosenCafeteria.ElementAt(0).IsHotMealServed != true &&
            addNewPacketViewModel.MealType == MealType.HotDinner)
            ModelState.AddModelError(nameof(Cafeteria),
                "This cafeteria does not sell hot meals");

        try
        {
            new FoodPacket
            {
                PickupTime = addNewPacketViewModel.PickupTime,
                PickupExpirationTime = addNewPacketViewModel.PickupExpirationTime
            };
        }
        catch (Exception e)
        {
            ModelState.AddModelError(nameof(FoodPacket),
                e.Message);
        }

        if (!ModelState.IsValid)
        {
            var products = _productRepository.GetAllProducts();
            var checkboxOptions = products.Select(product => new CheckboxOption
                {Id = product.Id, Name = product.Name, IsChecked = false}).ToList();
            addNewPacketViewModel.ProductsAsCheckboxOption = checkboxOptions;
            return View(addNewPacketViewModel);
        }

        // get chosen products and add new packet to database
        var chosenProducts = addNewPacketViewModel.IdsOfChosenProducts
            .Select(id => _productRepository.GetProductById(id)).ToList();
        var foodPacket = new FoodPacket
        {
            City = addNewPacketViewModel.City,
            Location = worker.CafeteriaLocation,
            IsCollected = false,
            MealType = addNewPacketViewModel.MealType,
            Name = addNewPacketViewModel.Name,
            PickupTime = addNewPacketViewModel.PickupTime,
            PickupExpirationTime = addNewPacketViewModel.PickupExpirationTime,
            Products = chosenProducts,
            Price = addNewPacketViewModel.Price
        };
        foodPacket.SetCorrectAgeIndication();
        _foodPacketRepository.AddFoodPacket(foodPacket);

        return RedirectToAction("Index");
    }

    [HttpGet]
    public RedirectToActionResult DeletePacketById(int id)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        if (foodPacket.ReservedBy != null)
        {
            TempData["PacketIsReserved"] = "Apologies, but this packet has since been reserved by a student";
            return RedirectToAction("Index");
        }

        _foodPacketRepository.DeleteFoodPacket(foodPacket);
        return RedirectToAction("Index", "WorkerFoodPacket");
    }

    [HttpGet]
    public IActionResult EditPacketById(int id)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        var products = _productRepository.GetAllProducts();
        var checkboxOptions = products.Select(product => new CheckboxOption
            {Id = product.Id, Name = product.Name, IsChecked = foodPacket.Products.Contains(product)}).ToList();

        if (foodPacket.ReservedBy != null)
        {
            TempData["PacketIsReserved"] = "Apologies, but this packet has since been reserved by a student";
            return RedirectToAction("Index");
        }

        return View(new EditPacketViewModel
        {
            Id = id,
            City = (CafeteriaCityEnum) foodPacket.City,
            Name = foodPacket.Name,
            Price = (int) foodPacket.Price,
            Is18Plus = (bool) foodPacket.Is18Plus,
            MealType = (MealType) foodPacket.MealType,
            PickupTime = (DateTime) foodPacket.PickupTime,
            PickupExpirationTime = (DateTime) foodPacket.PickupExpirationTime,
            ProductsAsCheckboxOption = checkboxOptions
        });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> EditPacketById(EditPacketViewModel editPacketViewModel)
    {
        // figure out which single cafeteria was chosen
        var user = await _userManager.GetUserAsync(HttpContext.User);
        var worker = _cafeteriaWorkerRepository.GetCafeteriaWorkerByEmail(user.Email);
        var chosenCafeteria = _cafeteriaRepository.GetAllCafeterias().Where(c =>
            c.City == editPacketViewModel.City && c.CafeteriaLocation == worker.CafeteriaLocation);
        var enumerable = chosenCafeteria as Cafeteria[] ?? chosenCafeteria.ToArray();
        if (enumerable.Length > 1 || !enumerable.Any())
            ModelState.AddModelError(nameof(Cafeteria),
                "No cafeteria matched, or more than 1 cafeteria matched the combination of the chosen city and your location");

        var foodPacketChecking = _foodPacketRepository.GetFoodPacketById(editPacketViewModel.Id);
        if (foodPacketChecking?.ReservedBy != null)
        {
            TempData["PacketIsReserved"] = "Apologies, but this packet has since been reserved by a student";
            return RedirectToAction("Index");
        }

        try
        {
            new FoodPacket
            {
                PickupTime = editPacketViewModel.PickupTime,
                PickupExpirationTime = editPacketViewModel.PickupExpirationTime
            };
        }
        catch (Exception e)
        {
            ModelState.AddModelError(nameof(FoodPacket),
                e.Message);
        }

        if (!ModelState.IsValid)
        {
            var products = _productRepository.GetAllProducts();
            var checkboxOptions = products.Select(product => new CheckboxOption
                {Id = product.Id, Name = product.Name, IsChecked = false}).ToList();
            editPacketViewModel.ProductsAsCheckboxOption = checkboxOptions;
            return View(editPacketViewModel);
        }

        // get chosen products and add new packet to database
        var chosenProducts = editPacketViewModel.IdsOfChosenProducts
            .Select(id => _productRepository.GetProductById(id)).ToList();
        var foodPacket = new FoodPacket
        {
            Id = editPacketViewModel.Id,
            City = editPacketViewModel.City,
            Location = worker.CafeteriaLocation,
            Is18Plus = editPacketViewModel.Is18Plus,
            IsCollected = false,
            MealType = editPacketViewModel.MealType,
            Name = editPacketViewModel.Name,
            PickupTime = editPacketViewModel.PickupTime,
            PickupExpirationTime = editPacketViewModel.PickupExpirationTime,
            Products = chosenProducts,
            Price = editPacketViewModel.Price
        };
        foodPacket.SetCorrectAgeIndication();
        _foodPacketRepository.UpdateFoodPacket(foodPacket);

        return RedirectToAction("Index");
    }

    public ViewResult ViewFoodPacketDetails(int id)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(id);
        return View(foodPacket);
    }
}
