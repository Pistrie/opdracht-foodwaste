using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Core.Domain;
using Core.DomainServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Moq;
using Portal.Controllers;
using Portal.Models;

namespace Portal.Tests;

public class WorkerFoodPacketControllerTests
{
    [Fact]
    public async void Index_Should_Return_List_Of_Lists_Of_Packets()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock cafeteria worker
        var worker = new CafeteriaWorker
        {
            Id = 1,
            CafeteriaLocation = CafeteriaLocationEnum.LA,
            EmployeeNumber = 1122,
            EmailAddress = "p.paaltjens@avans.nl"
        };
        cafeteriaWorkerRepository.Setup(x => x.GetCafeteriaWorkerByEmail("foo")).Returns(worker);

        // mock list of packets
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var packetList = new List<FoodPacket>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LA,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.Bread,
                Name = "Remaining paninis",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 5
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LD,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.HotDinner,
                Name = "Hot dinner",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 8
            }
        };
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetAllFoodPackets()).Returns(packetList);

        // Act
        var result = await sut.Index();

        // Assert
        var model = result.Model as List<IEnumerable<FoodPacket>>;
        var workerLocationPackets = model?.First();
        var allLocationPackets = model?.Last();
        Assert.Single(workerLocationPackets);
        Assert.Equal(2, allLocationPackets.Count());
        Assert.Equal(packetList.Where(fp => fp.Location == worker.CafeteriaLocation).OrderBy(x => x.PickupTime),
            workerLocationPackets);
        Assert.Equal(packetList.OrderBy(x => x.PickupTime), allLocationPackets);
    }

    [Fact]
    public async void Worker_Should_Be_Able_To_Add_A_FoodPacket()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock cafeteria worker
        var worker = new CafeteriaWorker
        {
            Id = 1,
            CafeteriaLocation = CafeteriaLocationEnum.LA,
            EmployeeNumber = 1122,
            EmailAddress = "p.paaltjens@avans.nl"
        };
        cafeteriaWorkerRepository.Setup(x => x.GetCafeteriaWorkerByEmail("foo")).Returns(worker);

        // mock list of cafeterias
        var cafeterias = new List<Cafeteria>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LA,
                IsHotMealServed = true
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LD,
                IsHotMealServed = false
            }
        };
        cafeteriaRepository.Setup(x => x.GetAllCafeterias()).Returns(cafeterias);

        // mock list of packets
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var packetList = new List<FoodPacket>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LA,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.Bread,
                Name = "Remaining paninis",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 5
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LD,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.HotDinner,
                Name = "Hot dinner",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 8
            }
        };
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetAllFoodPackets()).Returns(packetList);

        // mock product
        productRepositoryMock.Setup(x => x.GetProductById(1)).Returns(new Product
        {
            Id = 1,
            Alcoholic = false,
            Name = "Panini spicy chicken"
        });

        // Act
        var packet = packetList.First();
        var result = await sut.AddNewPacket(new AddNewPacketViewModel
        {
            City = (CafeteriaCityEnum) packet.City,
            Name = packet.Name,
            Price = (int) packet.Price,
            MealType = (MealType) packet.MealType,
            PickupTime = (DateTime) packet.PickupTime,
            PickupExpirationTime = (DateTime) packet.PickupExpirationTime,
            IdsOfChosenProducts = new List<int> {1}
        }) as RedirectToActionResult;

        // Assert
        Assert.NotNull(result);
    }

    [Fact]
    public void Worker_Should_Be_Able_To_Delete_A_FoodPacket()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock packet deletion
        var tomorrow = DateTime.Now.AddDays(1).Date;
        foodPacketRepositoryMock.Setup(x => x.GetFoodPacketById(1)).Returns(new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        });
        foodPacketRepositoryMock.Setup(x => x.DeleteFoodPacket(new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        }));

        // Act
        var result = sut.DeletePacketById(1);

        // Assert
        Assert.NotNull(result);
    }

    [Fact]
    public async void Worker_Should_Be_Able_To_Edit_A_FoodPacket()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock cafeteria worker
        var worker = new CafeteriaWorker
        {
            Id = 1,
            CafeteriaLocation = CafeteriaLocationEnum.LA,
            EmployeeNumber = 1122,
            EmailAddress = "p.paaltjens@avans.nl"
        };
        cafeteriaWorkerRepository.Setup(x => x.GetCafeteriaWorkerByEmail("foo")).Returns(worker);

        // mock list of cafeterias
        var cafeterias = new List<Cafeteria>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LA,
                IsHotMealServed = true
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LD,
                IsHotMealServed = false
            }
        };
        cafeteriaRepository.Setup(x => x.GetAllCafeterias()).Returns(cafeterias);

        // mock list of packets
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var packetList = new List<FoodPacket>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LA,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.Bread,
                Name = "Remaining paninis",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 5
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                Location = CafeteriaLocationEnum.LD,
                Is18Plus = false,
                IsCollected = false,
                MealType = MealType.HotDinner,
                Name = "Hot dinner",
                PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                Price = 8
            }
        };
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetAllFoodPackets()).Returns(packetList);

        // mock product
        productRepositoryMock.Setup(x => x.GetProductById(1)).Returns(new Product
        {
            Id = 1,
            Alcoholic = false,
            Name = "Panini spicy chicken"
        });

        // Act
        var packet = packetList.First();
        var result = await sut.EditPacketById(new EditPacketViewModel
        {
            City = (CafeteriaCityEnum) packet.City,
            Name = packet.Name,
            Price = (int) packet.Price,
            MealType = (MealType) packet.MealType,
            PickupTime = (DateTime) packet.PickupTime,
            PickupExpirationTime = (DateTime) packet.PickupExpirationTime,
            IdsOfChosenProducts = new List<int> {1}
        }) as RedirectToActionResult;

        // Assert
        Assert.NotNull(result);
    }

    [Fact]
    public void Worker_Cannot_Delete_FoodPacket_If_It_Has_Been_Reserved()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock packet
        var tomorrow = DateTime.Now.AddDays(1).Date;
        foodPacketRepositoryMock.Setup(x => x.GetFoodPacketById(1)).Returns(new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 18, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5,
            ReservedBy = new Student
            {
                Id = 1,
                Birthday = new DateTime(2000, 11, 23),
                EmailAddress = "foo",
                Name = "foo",
                PhoneNumber = "06 12345678",
                SchoolCity = CafeteriaCityEnum.Breda,
                StudentNumber = 1234567
            }
        });

        // Act
        var result = sut.DeletePacketById(1);

        // Assert
        Assert.NotNull(result);
        Assert.NotNull(sut.TempData["PacketIsReserved"]);
    }

    [Fact]
    public void FoodPacket_Should_Contain_List_Of_Products()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock products
        var products = new List<Product>
        {
            new()
            {
                Id = 1,
                Alcoholic = false,
                Name = "Panini spicy chicken"
            },
            new()
            {
                Id = 2,
                Alcoholic = false,
                Name = "Big Bowl of tomato soup"
            }
        };

        // mock foodpacket
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        };
        foodPacketRepositoryMock.Setup(fpr => fpr.GetFoodPacketById(1)).Returns(foodPacket);

        // add products and foodpacket together
        foodPacket.Products = products;
        foreach (var product in products) product.FoodPackets?.Add(foodPacket);

        // Act
        var result = sut.ViewFoodPacketDetails(1);

        // Assert
        var model = result.Model as FoodPacket;
        Assert.NotNull(result);
        Assert.NotEmpty(model?.Products);
    }

    [Fact]
    public async void FoodPacket_With_HotDinner_Should_Only_Be_Able_To_Be_Added_To_A_HotDinner_Cafeteria()
    {
        // Arrange
        var cafeteriaRepository = new Mock<ICafeteriaRepository>();
        var cafeteriaWorkerRepository = new Mock<ICafeteriaWorkerRepository>();
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<WorkerFoodPacketController>>();
        var productRepositoryMock = new Mock<IProductRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new WorkerFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            cafeteriaWorkerRepository.Object,
            productRepositoryMock.Object,
            cafeteriaRepository.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock cafeteria worker
        var worker = new CafeteriaWorker
        {
            Id = 1,
            CafeteriaLocation = CafeteriaLocationEnum.LA,
            EmployeeNumber = 1122,
            EmailAddress = "p.paaltjens@avans.nl"
        };
        cafeteriaWorkerRepository.Setup(x => x.GetCafeteriaWorkerByEmail("foo")).Returns(worker);

        // mock list of cafeterias
        var cafeterias = new List<Cafeteria>
        {
            new()
            {
                Id = 1,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LA,
                IsHotMealServed = false
            },
            new()
            {
                Id = 2,
                City = CafeteriaCityEnum.Breda,
                CafeteriaLocation = CafeteriaLocationEnum.LD,
                IsHotMealServed = false
            }
        };
        cafeteriaRepository.Setup(x => x.GetAllCafeterias()).Returns(cafeterias);

        // mock packet
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 2,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LD,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.HotDinner,
            Name = "Hot dinner",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 8
        };

        // mock product
        productRepositoryMock.Setup(x => x.GetProductById(1)).Returns(new Product
        {
            Id = 1,
            Alcoholic = false,
            Name = "Panini spicy chicken"
        });

        // mock AddNewPacketViewModel
        var addNewPacketViewModel = new AddNewPacketViewModel()
        {
            City = (CafeteriaCityEnum) foodPacket.City,
            Name = foodPacket.Name,
            Price = (int) foodPacket.Price,
            MealType = (MealType) foodPacket.MealType,
            PickupTime = (DateTime) foodPacket.PickupTime,
            PickupExpirationTime = (DateTime) foodPacket.PickupExpirationTime,
            IdsOfChosenProducts = new List<int> {foodPacket.Id}
        };

        // Act
        var result = await sut.AddNewPacket(addNewPacketViewModel) as ViewResult;

        // Assert
        var modelStateErrors = sut.ModelState.Select(x => x.Value.Errors)
            .Where(y => y.Count > 0).ToList();
        Assert.NotNull(result);
        Assert.Equal("This cafeteria does not sell hot meals",modelStateErrors[0][0].ErrorMessage);
    }
}
