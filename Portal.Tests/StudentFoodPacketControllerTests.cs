using System.Security.Claims;
using Core.Domain;
using Core.DomainServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Moq;
using Portal.Controllers;
using Portal.Models;

namespace Portal.Tests;

public class StudentFoodPacketControllerTests
{
    [Fact]
    public async void Index_Should_Return_Model_With_All_Available_FoodPackets()
    {
        // Arrange
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<StudentFoodPacketController>>();
        var studentRepositoryMock = new Mock<IStudentRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new StudentFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            studentRepositoryMock.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock student
        studentRepositoryMock.Setup(studentRepoMock => studentRepoMock.GetStudentByEmail("foo")).Returns(
            new Student
            {
                Id = 1,
                Birthday = new DateTime(2000, 11, 23),
                EmailAddress = "foo",
                Name = "foo",
                PhoneNumber = "06 12345678",
                SchoolCity = CafeteriaCityEnum.Breda,
                StudentNumber = 1234567
            });

        // mock list of packets
        var tomorrow = DateTime.Now.AddDays(1).Date;
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetAllFoodPackets()).Returns(
            new List<FoodPacket>
            {
                new()
                {
                    Id = 1,
                    City = CafeteriaCityEnum.Breda,
                    Location = CafeteriaLocationEnum.LA,
                    Is18Plus = false,
                    IsCollected = false,
                    MealType = MealType.Bread,
                    Name = "Remaining paninis",
                    PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                    PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                    Price = 5
                },
                new()
                {
                    Id = 2,
                    City = CafeteriaCityEnum.Breda,
                    Location = CafeteriaLocationEnum.LD,
                    Is18Plus = false,
                    IsCollected = false,
                    MealType = MealType.HotDinner,
                    Name = "Hot dinner",
                    PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                    PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                    Price = 8
                }
            });

        // Act
        var result = await sut.Index();

        // Assert
        var model = result.Model as StudentIndexViewModel;
        Assert.Equal(2, model?.FoodPackets.Count());
    }

    [Fact]
    public async void ReservedPackets_Should_Return_Model_With_FoodPackets_Reserved_By_Student()
    {
        // Arrange
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<StudentFoodPacketController>>();
        var studentRepositoryMock = new Mock<IStudentRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new StudentFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            studentRepositoryMock.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock list of packets
        var reservationStudent = new Student
        {
            Id = 1,
            Birthday = new DateTime(2000, 11, 23),
            EmailAddress = "foo",
            Name = "foo",
            PhoneNumber = "06 12345678",
            SchoolCity = CafeteriaCityEnum.Breda,
            StudentNumber = 1234567
        };

        var tomorrow = DateTime.Now.AddDays(1).Date;
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetAllFoodPackets()).Returns(
            new List<FoodPacket>
            {
                new()
                {
                    Id = 1,
                    City = CafeteriaCityEnum.Breda,
                    Location = CafeteriaLocationEnum.LA,
                    Is18Plus = false,
                    IsCollected = false,
                    MealType = MealType.Bread,
                    Name = "Remaining paninis",
                    PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                    PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                    Price = 5,
                    ReservedBy = reservationStudent
                },
                new()
                {
                    Id = 2,
                    City = CafeteriaCityEnum.Breda,
                    Location = CafeteriaLocationEnum.LD,
                    Is18Plus = false,
                    IsCollected = false,
                    MealType = MealType.HotDinner,
                    Name = "Hot dinner",
                    PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
                    PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
                    Price = 8
                }
            });

        // Act
        var result = await sut.ReservedPackets();

        // Assert
        var model = result.Model as IEnumerable<FoodPacket>; // as List<FoodPacket>;
        Assert.Equal(1, model?.Count());
    }

    [Fact]
    public void Packet_Should_Turn_18_Plus_If_Containing_Alcoholic_Products()
    {
        // Arrange
        var product = new Product
        {
            Alcoholic = true,
            Id = 1,
            Name = "Beer"
        };

        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Beverage,
            Name = "Beer",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        };

        product.FoodPackets = new List<FoodPacket> {foodPacket};
        foodPacket.Products = new List<Product> {product};

        // Act
        foodPacket.SetCorrectAgeIndication();

        // Assert
        Assert.Equal(true, foodPacket.Is18Plus);
    }

    [Fact]
    public async void Student_Should_Be_Able_To_Reserve_A_FoodPacket()
    {
        // Arrange
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<StudentFoodPacketController>>();
        var studentRepositoryMock = new Mock<IStudentRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new StudentFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            studentRepositoryMock.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock student
        var student = new Student
        {
            Id = 1,
            Birthday = new DateTime(2000, 11, 23),
            EmailAddress = "foo",
            Name = "foo",
            PhoneNumber = "06 12345678",
            SchoolCity = CafeteriaCityEnum.Breda,
            StudentNumber = 1234567
        };
        studentRepositoryMock.Setup(studentRepoMock => studentRepoMock.GetStudentByEmail("foo")).Returns(student);

        // mock foodpacket
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        };
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.GetFoodPacketById(1)).Returns(foodPacket);
        foodPacketRepositoryMock.Setup(foodPacketRepo => foodPacketRepo.SetReservation(foodPacket, student));

        // Act
        var result = await sut.ReserveFoodPacket(1);

        // Assert
        Assert.NotNull(result);
        foodPacketRepositoryMock.Verify(fpr => fpr.SetReservation(foodPacket, student), Times.Exactly(1));
    }

    [Fact]
    public void Student_Should_Not_Be_Able_To_Reserve_A_FoodPacket_If_Already_Reserved_For_Day_Of_Pickup()
    {
        // Arrange
        // mock foodpacket
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        };

        // mock studentindexviewmodel
        var studentIndexViewModel = new StudentIndexViewModel
        {
            UserReservations = new List<FoodPacket> {foodPacket}
        };

        // Act
        var result = studentIndexViewModel.IsReservationThatDay(foodPacket);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public async void FoodPacket_Cannot_Be_Reserved_If_It_Already_Has_A_Reservation()
    {
        // Arrange
        var foodPacketRepositoryMock = new Mock<IFoodPacketRepository>();
        var loggerMock = new Mock<ILogger<StudentFoodPacketController>>();
        var studentRepositoryMock = new Mock<IStudentRepository>();
        var userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null,
            null, null, null, null, null);

        var sut = new StudentFoodPacketController(
            loggerMock.Object,
            foodPacketRepositoryMock.Object,
            userManagerMock.Object,
            studentRepositoryMock.Object);

        // mock HttpContext
        sut.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, "foo")
                    }
                ))
            }
        };
        // create TempData, otherwise crash
        sut.TempData = new TempDataDictionary(sut.ControllerContext.HttpContext, Mock.Of<ITempDataProvider>());

        // mock user
        var user = new IdentityUser
        {
            UserName = "foo",
            Email = "foo",
            NormalizedEmail = "FOO"
        };
        userManagerMock.Setup(x => x.GetUserAsync(sut.ControllerContext.HttpContext.User))
            .Returns(Task.FromResult(user));

        // mock student
        var student = new Student
        {
            Id = 1,
            Birthday = new DateTime(2000, 11, 23),
            EmailAddress = "foo",
            Name = "foo",
            PhoneNumber = "06 12345678",
            SchoolCity = CafeteriaCityEnum.Breda,
            StudentNumber = 1234567
        };

        studentRepositoryMock.Setup(studentRepoMock => studentRepoMock.GetStudentByEmail("foo")).Returns(student);

        // mock packet
        var tomorrow = DateTime.Now.AddDays(1).Date;
        var foodPacket = new FoodPacket
        {
            Id = 1,
            City = CafeteriaCityEnum.Breda,
            Location = CafeteriaLocationEnum.LA,
            Is18Plus = false,
            IsCollected = false,
            MealType = MealType.Bread,
            Name = "Remaining paninis",
            PickupTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 17, 0, 0),
            PickupExpirationTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 19, 0, 0),
            Price = 5
        };
        foodPacketRepositoryMock.Setup(fpr => fpr.GetFoodPacketById(1)).Returns(foodPacket);
        foodPacketRepositoryMock.Setup(fpr => fpr.SetReservation(foodPacket, student));

        // set reservation
        foodPacket.ReservedBy = student;

        // Act
        var result = await sut.ReserveFoodPacket(1);

        // Assert
        Assert.NotNull(result);
        Assert.NotNull(sut.TempData["PacketIsReserved"]);
        foodPacketRepositoryMock.Verify(fpr => fpr.SetReservation(foodPacket, student), Times.Exactly(0));
    }
}
