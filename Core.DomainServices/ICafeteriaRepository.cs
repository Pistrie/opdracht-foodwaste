using Core.Domain;

namespace Core.DomainServices;

public interface ICafeteriaRepository
{
    public Cafeteria GetCafeteriaById(int id);

    public IEnumerable<Cafeteria> GetAllCafeterias();
}