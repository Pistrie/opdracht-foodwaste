using Core.Domain;

namespace Core.DomainServices;

public interface IStudentRepository
{
    public void AddStudent(Student student);

    public void UpdateStudent(Student student);

    public Student GetStudentById(int id);

    public Student GetStudentByEmail(string email);

    public IEnumerable<Student> GetAllStudents();
}