using Core.Domain;

namespace Core.DomainServices;

public interface IFoodPacketRepository
{
    public void AddFoodPacket(FoodPacket foodPacket);

    public void UpdateFoodPacket(FoodPacket updatedFoodPacket);

    public void SetReservation(FoodPacket foodPacket, Student reservedBy);

    public void ClearReservation(FoodPacket foodPacket);

    public FoodPacket GetFoodPacketById(int id);

    public IEnumerable<FoodPacket> GetAllFoodPackets();

    public void DeleteFoodPacket(FoodPacket foodPacket);
}
