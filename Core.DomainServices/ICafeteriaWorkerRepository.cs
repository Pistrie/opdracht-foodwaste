using Core.Domain;

namespace Core.DomainServices;

public interface ICafeteriaWorkerRepository
{
    public void AddCafeteriaWorker(CafeteriaWorker cafeteriaWorker);

    public void UpdateCafeteriaWorker(CafeteriaWorker cafeteriaWorker);

    public CafeteriaWorker GetCafeteriaWorkerById(int id);

    public CafeteriaWorker GetCafeteriaWorkerByEmail(string email);

    public IEnumerable<CafeteriaWorker> GetAllCafeteriaWorkers();
}