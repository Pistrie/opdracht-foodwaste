using Core.Domain;

namespace Core.DomainServices;

public interface IProductRepository
{
    public void AddProduct(Product product);

    public void UpdateProduct(Product product);

    public Product GetProductById(int id);

    public IEnumerable<Product> GetAllProducts();

    public void DeleteProduct(Product product);
}