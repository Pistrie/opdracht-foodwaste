using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models;

public class AuthenticationModel
{
    [Required] public string EmailAddress { get; set; }

    [Required] public string Password { get; set; }
}
