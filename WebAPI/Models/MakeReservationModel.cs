using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models;

public class MakeReservationModel
{
    [Required] public string UserEmail { get; set; }

    [Required] public int FoodPacketId { get; set; }
}
