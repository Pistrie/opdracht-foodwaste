using Core.Domain;
using Core.DomainServices;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class FoodPacketController : ControllerBase
{
    private readonly IFoodPacketRepository _foodPacketRepository;
    private readonly IStudentRepository _studentRepository;

    public FoodPacketController(IFoodPacketRepository foodPacketRepository, IStudentRepository studentRepository)
    {
        _foodPacketRepository = foodPacketRepository;
        _studentRepository = studentRepository;
    }

    [HttpGet(Name = "GetAllFoodPackets")]
    public IEnumerable<FoodPacket> Get()
    {
        var foodPackets = _foodPacketRepository.GetAllFoodPackets().ToArray();
        return foodPackets;
    }

    [HttpPost(nameof(MakeReservation))]
    public IActionResult MakeReservation([FromBody] MakeReservationModel makeReservationModel)
    {
        var foodPacket = _foodPacketRepository.GetFoodPacketById(makeReservationModel.FoodPacketId);
        var student = _studentRepository.GetStudentByEmail(makeReservationModel.UserEmail);

        // check if packet hasnt been reserved yet
        if (foodPacket.ReservedBy != null)
            return StatusCode(400, new {message = "This food packet has already been reserved"});

        // check if user is 18 if packet contains alcohol
        var userAgePlus18 = student.Birthday.AddYears(18);
        if ((bool) foodPacket.Is18Plus && userAgePlus18.Date <= foodPacket.PickupTime.Value.Date)
            return StatusCode(400, new {message = "This packet can only be reserved by adults"});

        // check if user doesnt have reservation on pickuptime
        var foodPackets = _foodPacketRepository.GetAllFoodPackets();
        var studentReservations = foodPackets.Where(p => p.ReservedBy == student).ToList();
        if (studentReservations.Exists(m => m.PickupTime.Value.Date == foodPacket.PickupTime.Value.Date))
            return StatusCode(400, new {message = "You can only have 1 reservation per day"});

        _foodPacketRepository.SetReservation(foodPacket, student);
        return Ok(new {message = "reservation made"});
    }
}
