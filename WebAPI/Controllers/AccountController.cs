using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class AccountController : ControllerBase
{
    private readonly SignInManager<IdentityUser> _signInManager;
    private readonly UserManager<IdentityUser> _userManager;

    public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
    {
        _userManager = userManager;
        _signInManager = signInManager;
    }

    [HttpPost(nameof(LogIn))]
    public async Task<IActionResult> LogIn([FromBody] AuthenticationModel authenticationModel)
    {
        var user = await _userManager.FindByEmailAsync(authenticationModel.EmailAddress);
        if (user != null)
        {
            await _signInManager.SignOutAsync();
            if ((await _signInManager.PasswordSignInAsync(user, authenticationModel.Password, false, false)).Succeeded)
                return Ok(user);
        }

        return BadRequest(new {message = "username or password incorrect"});
    }

    [HttpPost(nameof(LogOut))]
    public async Task<IActionResult> LogOut()
    {
        await _signInManager.SignOutAsync();
        return Ok(new {message = "user signed out"});
    }
}
