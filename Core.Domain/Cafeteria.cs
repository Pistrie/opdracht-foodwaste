namespace Core.Domain;

public class Cafeteria
{
    public int Id { get; set; }

    public CafeteriaCityEnum City { get; set; }

    public CafeteriaLocationEnum CafeteriaLocation { get; set; }

    public bool IsHotMealServed { get; set; }
}
