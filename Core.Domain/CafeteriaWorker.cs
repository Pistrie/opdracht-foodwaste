namespace Core.Domain;

public class CafeteriaWorker
{
    public int Id { get; set; }

    public int EmployeeNumber { get; set; }

    public string EmailAddress { get; set; }

    public CafeteriaLocationEnum CafeteriaLocation { get; set; }
}