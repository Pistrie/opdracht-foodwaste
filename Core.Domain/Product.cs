namespace Core.Domain;

public class Product
{
    public int Id { get; set; }

    public string Name { get; set; }

    public bool Alcoholic { get; set; }

    public byte[]? ProfilePicture { get; set; }

    public string? PictureFormat { get; set; }

    public ICollection<FoodPacket>? FoodPackets { get; set; }
}
