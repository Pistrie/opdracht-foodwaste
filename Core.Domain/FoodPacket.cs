using System.ComponentModel.DataAnnotations;

namespace Core.Domain;

public enum MealType
{
    Bread,
    [Display(Name = "Hot dinner")] HotDinner,
    Beverage
}

public class FoodPacket
{
    private DateTime? _pickupExpirationTime;
    private DateTime? _pickupTime;

    public int Id { get; set; }

    public string Name { get; set; }

    public ICollection<Product>? Products { get; set; }

    public CafeteriaCityEnum? City { get; set; }

    public CafeteriaLocationEnum? Location { get; set; }

    public DateTime? PickupTime
    {
        get => _pickupTime;
        set
        {
            var twoDaysInFuture = DateTime.Now.AddDays(2);
            if (value > twoDaysInFuture.Date)
                throw new ArgumentException("Pickup time cannot be in more than two days");
            if (value < DateTime.Now.Date)
                throw new ArgumentException("Pickup time cannot be in the past");
            _pickupTime = value;
        }
    }

    public DateTime? PickupExpirationTime
    {
        get => _pickupExpirationTime;
        set
        {
            var twoDaysInFuture = DateTime.Now.AddDays(2);
            if (value > twoDaysInFuture.Date)
                throw new ArgumentException("Pickup expiration time cannot be in more than two days");
            if (value < DateTime.Now.Date)
                throw new ArgumentException("Pickup expiration time cannot be in the past");
            _pickupExpirationTime = value;
        }
    }

    public bool? Is18Plus { get; set; }

    public int? Price { get; set; }

    public MealType? MealType { get; set; }

    public Student? ReservedBy { get; set; }

    public bool IsCollected { get; set; }

    public void SetCorrectAgeIndication()
    {
        Is18Plus = (Products ?? throw new InvalidOperationException()).FirstOrDefault(p => p.Alcoholic) != null;
    }
}
