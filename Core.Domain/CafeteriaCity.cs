namespace Core.Domain;

public enum CafeteriaCityEnum
{
    Breda,
    DenBosch,
    Tilburg
}