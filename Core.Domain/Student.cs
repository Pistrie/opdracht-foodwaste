namespace Core.Domain;

public class Student
{
    private DateTime _birthDay;

    public int Id { get; set; }

    public string Name { get; set; }

    public DateTime Birthday
    {
        get => _birthDay;
        set
        {
            var studentBirthdayPlus16 = _birthDay.AddYears(16);
            if (studentBirthdayPlus16.Date <= DateTime.Now.Date)
                _birthDay = value;
            else if (_birthDay.Date >= DateTime.Now.Date)
                throw new InvalidOperationException("Birthday cannot be in the future");
            else
                throw new InvalidOperationException("Student must be at least 16 years old");
        }
    }

    public int StudentNumber { get; set; }

    public string EmailAddress { get; set; }

    public CafeteriaCityEnum SchoolCity { get; set; }

    public string PhoneNumber { get; set; }
}
